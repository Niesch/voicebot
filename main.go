package main

import (
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/thoj/go-ircevent"
	"io/ioutil"
	"os"
	"strings"
)

type Config struct {
	Server    string
	Admins    []string
	Dickheads []string
	Channels  []string
	Realname  string
	Nick      string
}

func main() {
	path := flag.String("config", "config.json", "path to the configuration file")
	flag.Parse()
	f, err := os.Open(*path)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	d := json.NewDecoder(f)
	conf := &Config{}
	err = d.Decode(conf)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	f.Close()

	i := irc.IRC(conf.Nick, conf.Realname)
	i.UseTLS = true
	i.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	err = i.Connect(conf.Server)
	if err != nil {
		fmt.Println(err)
	}
	i.AddCallback("001", func(e *irc.Event) {
		for _, c := range conf.Channels {
			i.Join(c)
		}
	})
	i.AddCallback("JOIN", func(e *irc.Event) {
		grant := true
		for _, d := range conf.Dickheads {
			if strings.ToLower(e.Nick) == strings.ToLower(d) {
				grant = false
				fmt.Println("Not granting voice to " + e.Nick + " on the grounds of being a dickhead.")
			}
		}

		if grant {
			fmt.Println("Granting voice to " + e.Nick)
			i.Mode(e.Arguments[0], "+v", e.Nick)
		}
	})

	i.AddCallback("PRIVMSG", func(e *irc.Event) {
		go func(e *irc.Event) {
			if !strings.HasPrefix(e.Message(), "!") {
				return
			}

			access := false
			for _, admin := range conf.Admins {
				if strings.ToLower(admin) == strings.ToLower(e.Nick) {
					access = true
				}
			}
			if !access {
				return
			}

			input := strings.TrimPrefix(e.Message(), "!")
			arr := strings.Split(input, " ")

			switch arr[0] {
			case "ignorera":
				if len(arr) < 2 {
					return
				}
				conf.Dickheads = append(conf.Dickheads, arr[1])
				i.Privmsg(e.Arguments[0], fmt.Sprintf("Jag kommer inte ge %s mer +v.", arr[1]))
			case "förgiv":
				if len(arr) < 2 {
					return
				}
				for ind, d := range conf.Dickheads {
					if strings.ToLower(d) == strings.ToLower(arr[1]) {
						conf.Dickheads = append(conf.Dickheads[:ind], conf.Dickheads[ind+1:]...)
						i.Privmsg(e.Arguments[0], fmt.Sprintf("Du är förlåten, %s!", arr[1]))
					}
				}
			case "spara":
				fmt.Println("Attempting to write to file.")
				j, err := json.Marshal(conf)
				if err != nil {
					i.Privmsg(e.Arguments[0], "Något gick fel, se loggen.")
					fmt.Println(err)
				}
				err = ioutil.WriteFile("config.json", j, 0644)
				if err != nil {
					i.Privmsg(e.Arguments[0], "Något gick fel, se loggen.")
					fmt.Println(err)
				}
			}
		}(e)

	})
	i.Loop()
}
